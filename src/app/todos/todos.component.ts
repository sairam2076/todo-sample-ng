import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos=[
    {
      label: ' WT record work' ,
      done: true,
      priority:1
    },
  
    {
      label: 'Assignment' ,
      done: false,
      priority:2
    },
  
    
  
    {
      label: 'Shopping' ,
      done: false,
      priority:3
    },
  
  ];
  
  
  addTodo(newTodoLabel){
    var newTodo={
      label:newTodoLabel,
      priority:1,
      done: false
    };
    this.todos.push(newTodo);
  }
  
  deleteTodo(todo){
    this.todos = this.todos.filter( t => t.label !==todo.label );
  }
 constructor() { 

  }

  ngOnInit() {
    
  }

}
